# Rifacimento del sistema informativo dell'ITIS A. Rossi

__Lupascu Cristian Nicolae, Nigro Federico, Nechifor Alexandru, Zabeo Alberto__

ITIS Rossi 2019

## Analisi della realtà di fatto
### Requisiti
Il committente del progetto di cablaggio è l’istituto A. Rossi ubicato in Via Legione Gallieno, 52, 36100 Vicenza VI.
Per il rifacimento della struttura si avanzano le seguenti proposte:

    * Garantire la sicurezza della rete interna da possibili minacce, sia interne che esterne
    * utilizzo di tecnologie avanzate quali per istanza fibra ottica e “dockerizzazione” dei sistemi informatici vigenti.
    * creazione di una rete affidabile di comunicazione mediante l’utilizzo di telefoni con tecnologia VoIP
    * fornire nelle classi, ove necessario, degli Access Point per l’accesso ad internet al fine di realizzare un modello di classe 2.0

## Contenuti
  - Rete
  - Proxy su kubernetes
  - Virtualizzazione OS
  - Monitoraggio
  - Analisi dei costi
  - Rete VoIP
  - Topologia

### Rete e sicurezza

Eseguendo il comando ```netstat -r```, abbiamo scoperto che all'interno della scuola è presento solo una rete, ovvero ```172.18.0.0 255.255.0.0```. La nostra proposta è quella di assegnare una subnet /24 per ogni laboratorio e di raggruppare tutti i PC delle aule in un'altra subnet /24, così come il firewall e il proxy server. Anche la rete wireless disporrà della propria subnet /24. Facendo così aumenterà la sicurezza all'interno della rete, in quanto potranno essere applicate delle policy di restrizione alle singole sottoreti e non all'intera rete, rendendo più semplice l'amministrazione di essa.


### Proxy

Per quanto riguarda il proxy server, al momento ne è presente uno solo, il che è problematico qualora non dovesse funzionare correttamente a seguito di sovraccarico.
Abbiamo pensato dunque di creare un cluster kubernetes contententi molteplici istanze di squid proxy, che verranno replicate a seconda del carico di lavoro del server. 
Abbiamo utilizzato l'immagine docker ```wernight/squid```, con il seguente Dockerfile:
```docker
FROM alpine:3.5

RUN set -x \

 && apk add --no-cache squid curl \

 && curl -Lo /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.1.3/dumb-init_1.1.3_amd64 \
 && chmod +x /usr/local/bin/dumb-init \

 && apk del curl

EXPOSE 3128

ENTRYPOINT ["dumb-init"]
CMD ["squid", "-N"]
```

Successivamente abbiamo creato i due file ```deployment.yaml```
```yaml
apiVersion: v1
kind: Deployment
metadata:
  labels:
    app: squid
  name: squid
spec:
  replicas: 5
  template:
    metadata:
      labels:
        app: squid-app
    spec:
      containers:
        - name: squid
          image: wernight/squid
          imagePullPolicy: Always
          ports:
          - containerPort: 3128
            protocol: TCP
          volumeMounts:
            - mountPath: /var/spool/squid
              name: squid-cache
            - mountPath: /var/log/squid
              name: squid-log
          livenessProbe:
            tcpSocket:
              port: 3128
            initialDelaySeconds: 40
            timeoutSeconds: 4
      volumes:
        - name: squid-cache
          emptyDir: {}
        - name: squid-log
          emptyDir: {}
```
e ```service.yaml```
```yaml
apiVersion: v1
kind: Service
metadata:
  name: squid
  labels:
    name: squid
spec:
  type: LoadBalancer
  ports:
    - port: 3128
      targetPort: 3128
      protocol: TCP
  selector:
    name: squid-app
```

Il deployment del cluster viene fatto utilizzando il comando
```bash
kubectl create -f deployment.yaml -f service.yaml
```

Verrà così creato il pod ```squid```. Per aggiungere la funzionalità di scalabilità orizzontale, verrà eseguito il comando
```bash
kubectl autoscale deployment squid --min=1 --max=5 --cpu-percent=50
```
In questo modo il numero di repliche del pod verrà aumentato o diminuito entro i limiti stabiliti in modo tale da far mantere a tutti i pod un utilizzo medio di CPU del 50%.
Per il momento non sono previsti sistemi di autenticazione.

### Virtualizzazione OS


Inizialmente è stata pensata la virtualizzazione dei computer in modo tale da poter creare un'istanza di un SO Windows 10 per ogni studente e docente,in modo tale da potervi accedere indipendentemente dal laboratorio o classe in cui si trova. Purtroppo questa soluzione non è conveniente in termini di costi e risorse in quanto avremmo dovuto creare circa 1200 macchine virtuali.

Abbiamo dunque pensato di creare un container docker per studente, partendo da un'immagine base di Windows 10. Ciò avrebbe comportato un minore utilizzo di risorse del server. Purtroppo però le immagini docker di Windows 10, per il momento, non supportano l'interfaccia grafica, quindi non si sarebbe rivelata utile come soluzione.

La soluzione scelta dunque è piuttosto simile a quella presente al momento: Verranno sostituiti i pc più vecchi e installato Windows 10 su quelli che utilizzano versioni precedenti (in molti laboratori è presente Windows 7). Per quanto riguarda gli account degli studenti e dei docenti, verranno inseriti nel dominio ```DIDATTICA```, in modo da poter avere un unico account nel quale contenere tutti i progetti.


### Monitoraggio


* Per la gestione dei dispositivi presenti nella rete interna da parte dei tecnici verrà fornita la possibilità di utilizzare un insieme di servizi di rete comunemente noto come Active Directory.
Essa sfrutta la modalità Sigle Sign-ON(SSO). Questa permette all’utente,previo accesso al dominio, di accedere alla risorse disponibili in rete limitate a seconda del suo ruolo tramite policy di sicurezza.

* per la gestione centralizzata del wifi, seguita ad possibili individuazione di errori e risoluzioni tempestive, si richiede l’utilizzo di software come EAP Controller

* Per la gestione di rete è necessario fornire un protocollo per la ricezione dei log.In questo caso di adotta il protocollo SYSLOG ed eventuali software collegati ad esso ad esempio Nagios SysLog.


* Per il controllo,la messa in sicurezza e il troubleshooting inerente la qualità della trasmissione(QoS) dei telefono VoIP è necessario utilizzare il software SolarWinds VoIP.

* per la trasmissione di dati statistici sulla rete si utilizza il protocollo Netflow. Esso possiede l'apparato (Metering Process) che osserva i flussi di traffico in un (Observation Point) 
Ogni tanto scarica su un server (Collector) i dati relativi ai flussi osservati 




### Analisi dei costi
E' necessario fornire un'analisi dettagliata dei costi di hw e sw mediante un semplice foglio di calcolo al fine di prevedere il costo finale in seguito ad un certo budget iniziale
Successivamente verranno effettuati 3 test di calcolo: il primo sarà effettuato dopo la prima analisi e realizzazione del cablaggio iniziale, la seconda dopo la analisi di eventuali punti critici e la terza dopo la relazione finale

|Dispositivo|Marca|Modello|Costo/unità|Quantià|Totale|Costo totale|
|----|----|----|----|----|----|----|
|Telefono VoIP|GrandStream|GPX-1610|40,66€|29|1179,14€|74547,14€|
|PC Desktop|Dell|Optiplex 3010|268€|201|53868€|
|Licenza Windows|Microsoft|Windows 10 pro|260€|75|19500€|

### Rete voip
L'istituto necessita di una rete VoIP al fine di facilitare e velocizzare le comunicazioni tra i vari laboratori.
<br>

## Analisi della rete e dei dispositivi
Prima di proporre modifiche alla rete esistente bisogna ovviamente conoscerla. Abbiamo dunque individuato 3 importanti punti da analizzare della rete esistente:
*  Numero di omputer per laboratorio
*  Modalità e qualità della connettività della LAN con Internet
*  Infrastruttura di rete e intermediary devices

### Numero di computer per laboratorio
Al fine di dar il via all'analisi dei costi e futuro cablaggio dell'istituto è stato necessario avere a disposizione il numero di computer preesistenti nella scuola.
Sono stati effettuati vari tentativi al fine di determinarne il numero esatto.
Di seguito vengono elencati:
<br>
1.Primo script in c#  5/05/2019<br>
```csharp
try {
    NetworkInterface iFace = NetworkInterface
            .getByInetAddress(InetAddress.getByName(YourIPAddress));

    for (int i = 0; i <= 255; i++) {

        // build the next IP address
        String addr = YourIPAddress;
        addr = addr.substring(0, addr.lastIndexOf('.') + 1) + i;
        InetAddress pingAddr = InetAddress.getByName(addr);

        // 50ms Timeout for the "ping"
        if (pingAddr.isReachable(iFace, 200, 50)) {
            Log.d("PING", pingAddr.getHostAddress());
        }
    }
} catch (UnknownHostException ex) {
} catch (IOException ex) {
}
```
2.
```csharp
    
        public static uint ParseIP(string ip)
        {
            byte[] b = ip.Split('.').Select(s => Byte.Parse(s)).ToArray();
            if (BitConverter.IsLittleEndian) Array.Reverse(b);
            return BitConverter.ToUInt32(b, 0);
        }

        public static string FormatIP(uint ip)
        {
            byte[] b = BitConverter.GetBytes(ip);
            if (BitConverter.IsLittleEndian) Array.Reverse(b);
            return String.Join(".", b.Select(n => n.ToString()));
        }




        static void Main(string[] args)
        {
            string StartIP = "192.168.1.0"//indirizzo iniziale da cui iniziare lo scan;
            int IPCount =(int) Math.Pow(10,3) //indica il numero di computer successivi all'ip definito da testare;

            uint n = ParseIP("192.168.1.0");
            string[] range = new string[IPCount];
            for (uint i = 0; i < IPCount; i++)
            {
                range[i] = FormatIP(n + i);
                Console.WriteLine(range[i]);
            }


            List<Task<PingReply>> pingTasks = new List<Task<PingReply>>();
            foreach (var add in range)
            {
                pingTasks.Add(PingAsync(add));
            }

            //attendo il completamento dei task
            Task.WaitAll(pingTasks.ToArray());

          
            foreach (var pingTask in pingTasks)
            {
                //indica se è timeout o success
                Console.WriteLine(pingTask.Result.Status);
            }
            Console.ReadLine();
        }

        static Task<PingReply> PingAsync(string address)
        {
            var tcs = new TaskCompletionSource<PingReply>();
            Ping ping = new Ping();
            ping.PingCompleted += (obj, sender) =>
            {
                tcs.SetResult(sender.Reply);
            };
            ping.SendAsync(address, new object());
            return tcs.Task;
        }
```

3. Terza realizzazione di uno script
Il primo ipotizzato per il conteggio mediante Active Directory è il seguente 5/05/2019
```powershell
Get-ADComputer -Filter * -properties *|select Name, DNSHostName, OperatingSystem, LastLogonDate
```

4. Realizzazione di un semplice script Powershell mediante Active Directory<br>   6/05/2019
  ```powershell
$Computers = (Get-ADComputer -Filter *).count
$Workstations = (Get-ADComputer -LDAPFilter "(&(objectClass=Computer)(!operatingSystem=*server*))" -Searchbase (Get-ADDomain).distinguishedName).count
$Servers = (Get-ADComputer -LDAPFilter "(&(objectClass=Computer)(operatingSystem=*server*))" -Searchbase (Get-ADDomain).distinguishedName).count

Write-Host "Computers =      "$Computers
Write-Host "Workstions =     "$Workstations
Write-Host "Servers =        "$Servers 
```
Il seguente script non permette, in seguito ad un'attenta analisi il conteggio dei computer spenti e non è possibile in termini di utilizzo in quanto non è possibile ottenere dei privilegi di admin.

Gli script fino a quel momento realizzati risultano inefficaci in quanto senza privilegi è impossibile installare un singolo modulo per powershell.
Inoltre, La scuola possiede un sistema di sicurezza che non permette allo studente il comando ping tra due computer visibili invece tramite uno scan arp.

5. Scan con nmap   10/05/2019 <br>
Un successivo tentativo è stato effettuato mediante semplici scan nmap
Anche qui il problema dei computer spenti sussiste.
    6. Script in c# eseguito all'avvio del pc con ruolo di server e successivamente ogni ora
Aggiungere un @reboot cron task da terminale
```bash
	crontab -e
```
Aggiungere la seguente linea se si vuole eseguire lo script solo al reboot
```bash
	@reboot /path/per/lo/script
```
Altrimenti
```bash
	0 * * * * /path/per/lo/script
```
Questo lancierà lo script ogni ora al minuto 0 es 1.00 2.00 3.00 4.00 ecc..
Creare il file csExec per la compilazione ed esecuzione automatica di script
```bash
if [ ! -f "$1" ]; then
dmcs_args=$1
shift
else
dmcs_args=""
fi
script=$1
shift
input_cs="$(mktemp)"
output_exe="$(mktemp)"
tail -n +2 $script > $input_cs
dmcs $dmcs_args $input_cs -out:${output_exe} && mono $output_exe $@                                                                          
rm -f $input_cs $output_exe
```
Realizzare lo script cs per l'individuazione dei computer accessi
```csharp
#!/usr/bin/csexec                                                                                                                     
using System;                                                                                                                                
public class Program                                                                                                                         
{ 
public List<String> risultati = new List<String>();
public static uint ParseIP(string ip)
        {
            byte[] b = ip.Split('.').Select(s => Byte.Parse(s)).ToArray();
            if (BitConverter.IsLittleEndian) Array.Reverse(b);
            return BitConverter.ToUInt32(b, 0);
        }

        public static string FormatIP(uint ip)
        {
            byte[] b = BitConverter.GetBytes(ip);
            if (BitConverter.IsLittleEndian) Array.Reverse(b);
            return String.Join(".", b.Select(n => n.ToString()));
        }

public List<string> init()
{
  risultati.clear();
string StartIP = "192.168.1.0"//indirizzo iniziale da cui iniziare lo scan è necessario modificarlo;
            int IPCount =(int) Math.Pow(10,3) //indica il numero di computer successivi all'ip definito da testare;

            uint n = ParseIP("192.168.1.0");
            string[] range = new string[IPCount];

}

 public static async Task LoopAndCheckPingAsync(List<string> addresses)
        {

            while (true)
            {
                var ping = new Ping();
                var pingTasks = addresses.Select(address => ping.SendPingAsync(address));

                await Task.WhenAll(pingTasks);

                StringBuilder pingResultBuilder = new StringBuilder();

                foreach (var pingReply in pingTasks)
                {
                    pingResultBuilder.Append(pingReply.Result.Address);
                    pingResultBuilder.Append("    ");
                    pingResultBuilder.Append(pingReply.Result.Status);
                    pingResultBuilder.Append("    ");
                    pingResultBuilder.Append(pingReply.Result.RoundtripTime.ToString());
                    pingResultBuilder.AppendLine();
                }

                Console.WriteLine(pingResultBuilder.ToString());
                risultati.Append(pingResultBuilder.ToString());



                await Task.Delay(TimeSpan.FromMinutes(5));
            }
        }


        public void mergeFile(String filePath1, String stringSearch)
        {
          string path = filePath1;

if (!File.Exists(path)) // se non esiste
{
    using (StreamWriter sw = File.CreateText(path))
    {
        foreach (var line in addresses.Items)
        {
            sw.WriteLine((String)line +"\n");
            
        }
    }
}
else
{
    StreamWriter sw = File.AppendText(path);

    foreach (var line in addresses)
    {
      if(!checkExistsLine(path, line))
        sw.WriteLine((String)line +"\n");
    }
    sw.Close();
}
        }


        static void Main()
        {
            List<string> addresses = init();
   
            Task.Factory.StartNew(() => LoopAndCheckPingAsync(addresses));
            LoopAndCheckPingAsync();
        }

        public void checkExistsLine(String path, String line)
        => File.ReadLines(path).Contains(line); // ritorna se esiste una linea

}        
```                                                                                                          
  <br> 7. Individuazione dell'ultima accensione 18/05/2019<br>
Un successivo tentantivo è stato possibile mediante l'analisi di scan del software nmap, ovvero dato uno scan all'istante t si salvano in codice xml i dati e i dispositivi trovati. Ogni t+1 istante viene effettuato un successivo scan in cui nel caso si individuino nuovi computer non presenti nel precedente scan secondo l'analisi di parametri quali(ultimo login, ip, ecc), essi vengono aggiunti al codice xml. Con questa procedura dovrebbe essere possibile ottenere il numero teorico di computer, con una precisione quasi vicina al numero reale/concreto di essi(si consideri i possibili computer quasi sempre o al 99% spenti)




    8. Si ritiene più conveniente la soluzione 6 in quanto è possibile contare i pc attivi e ogni ora (chiaramente un parametro modificabile ) il programma verrà eseguito.
     Come funziona il programma del punto 6 



        1. effettua dei ping dato un range in multithreading cosi facendo si aumenta la velocità di esecuzione e di calcolo distribuendo il lavoro  <br> 


        2. verifica se il file in cui verrano salvati i pc presenti è gia' stato creato(in caso negativo lo crea e aggiunge i risultati della scansione).  <br> 

Nel caso positivo controlla se i dati della scansione all'ora h risultano uguali a quelli dell'ora h-1 se la risposta è negativa allora gli aggiunge.

9. Stiamo provando a creare un programma in C# che permetta non solo di eseguire la scansione dei pc periodicamente, ma anche di inserire all'interno di un database gli indirizzi ip e la data in cui sono stati trovati, in modo tale da avere una stia approssimativa del loro periodo di utilizzo.

