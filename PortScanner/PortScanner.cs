/* A simple multi-threaded Port Scanner
 * Author: Munir Usman http://munir.wordpress.com
 * Contact: Munir.Usman@gmail.com 
 */

using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;
using System.IO;

namespace PortScanner
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<string> host = new Queue<string>();
            for (int x = 1; x < 255; x++)
            {
                host.Enqueue($"192.168.178.{x}");
            }
            PortScanner ps = new PortScanner(host);
            ps.host = host;
            ps.start();

        }
    }
}
