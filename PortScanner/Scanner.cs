ï»¿using MySql.Data.Common;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace PortScanner
{
    public class PortScanner
    {
        private string fileName = Path.Combine(Directory.GetCurrentDirectory(), "\\scansione.txt");
        public Queue<string> host { get; set; }
        private int _hostLength;
        private List<int> portList;
        public List<string> ipAddesses { get; }

        public PortScanner(Queue<string> host)
        {
            this.host = new Queue<string>();
            this.portList = new List<int>();
            portList.Add(80); //HTTP
            portList.Add(53); //DNS
            portList.Add(25); //DNS
            portList.Add(135); //Windows RPC
            _hostLength = host.Count;

            this.ipAddesses = new List<string>();
        }

        public void start()
        {
            Console.WriteLine("\n\nNuova scansione");
            for (int i = 0; i < _hostLength; i++)
            {
                Thread th = new Thread(new ThreadStart(run));
                th.Start();
            }
        }
        public void run()
        {
            string ip;

            lock (host)
            {
                ip = host.Dequeue();
                host.Enqueue(ip);
            }

            foreach (var port in portList)
            {
                Thread th = new Thread(new ThreadStart(() => runPortScan(ip, port)));
                th.Start();
            }
        }

        public void runPortScan(string ip, int port)
        {
            lock (Console.Out)
            {
                Console.WriteLine($"Inizio scansione porta: {port} indirizzo {ip}");
            }
            TcpClient tcp = null;
            try
            {
                tcp = new TcpClient(ip, port);
                lock (ipAddesses)
                {
                    ipAddesses.Add(ip);

                    using (StreamWriter fs = File.AppendText(fileName))
                    {
                        string a = $"{ip}:{port} {DateTime.Now}\n";
                        fs.Write(a, 0, a.Length);
                    }

                    //TODO sistemare la connessione (accesso negato)

                    /*MySqlConnection con = new MySqlConnection("Server=localhost;Port=3306;Database=portscan;Uid=lupascu;password=lupascu");
                    MySqlCommand command = new MySqlCommand();
                    command.CommandText = $"INSERT INTO pc(address,port,date) VALUES(\"{ip}\",\"{port}\",{DateTime.Now})";
                    con.Open();
                    command.ExecuteNonQuery();
                    con.Close();*/

                    
                }
                Thread.Sleep(100);
                Thread.CurrentThread.Interrupt();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                try
                {
                    tcp.Close();
                }
                catch { }
            }
        }
    }
}
